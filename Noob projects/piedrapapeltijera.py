import random

def main():
    print(play())

def play():
    user = input("Qué elegís, pa? 'Piedra', 'Papel' o 'Tijera'?: \n")
    computer = random.choice(['Piedra', 'Papel', 'Tijera'])

    if user == computer:
        return f"It's a tie. You've chosen {user} and the computer has chosen {computer}."

    if is_win(user, computer):
        return f"You won! You've chosen {user} and the computer has chosen {computer}."
    
    return f"You lost, bitch. You've chosen {user} and the computer has chosen {computer}."
    
def is_win(player, opponent):
    # Return true if player wins
    if (player == 'Piedra' and opponent == 'Tijera') or (player == 'Tijera' and opponent == 'Papel') or (player == 'Papel' and opponent == 'Piedra'):
        return True

if __name__ == "__main__":
    main()